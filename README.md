# Performance Manager Vacation Days

PM vacation days is a Chrome extension for showing vacation days in Performance Manager as days instead of hours.

## Installation Instructions

1. Clone this repo or [download](https://bitbucket.org/Theyssen/pm-vacation-days/get/master.zip) the code as a zip. 
2. In Chrome, open the Extensions settings. (click the dots, Extensions, Manage extensions)
3. On the Extensions settings page, click the "Developer Mode" checkbox in the top right corner.
4. Click the now-visible "Load unpacked extension…" button. Navigate to the directory where you cloned the repo.
5. The *PM vacation days* extension should now be visible in your extensions list.
