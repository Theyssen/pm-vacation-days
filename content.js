function replaceHours(element) {
  const hoursPerDay = 8;
  const list = element.querySelectorAll(
    '#idEssContainer---main--accountTable-tblBody .sapMObjectNumberText' +
    ', #idEssContainer---main--balancesList .sapMSLIInfo' +
    ', #idEssContainer---main--timeTypeBalanceTable-tblBody .sapMText');
  list.forEach(el => {
    if (el.dataset.days !== 'true') {
      el.dataset.days = 'true';
      const text = el.innerText;
      const hours = parseFloat(text.split(' ')[0]);
      if (isNaN(hours)) return;
      let newText = `${text} (${Math.floor(hours / hoursPerDay)}d`;
      if (hours % hoursPerDay > 0) {
        newText += ` ${Math.round(hours % hoursPerDay * 100) / 100}u`
      }
      newText += ')';
      el.innerText = newText;
    }
  });
}

MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

const observer = new MutationObserver((mutations) => {
  const filteredMutations = mutations.filter(m => m.target.tagName !== 'HEAD' && m.addedNodes.length > 0);
  filteredMutations.forEach(mutation => {
    mutation.addedNodes.forEach(node => {
      if (node.nodeType === 1) {
        replaceHours(node);
      }
    });
  });
});

observer.observe(document, {
  subtree: true,
  childList: true
});
